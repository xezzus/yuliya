<?php
return [function($dateStart,$dateStop){
  $plan = $this->plan->get($dateStart,$dateStop);
  $ids = array_column($plan,'idtimeline','idtimeline');
  $fact = $this->fact->get($dateStart,$dateStop);
  $timeline = $this->timeline->getLineByIds($ids);
  foreach($plan as $key=>$value){
    $timeline[$value['idtimeline']]['plans'][] = $value;
  }
  foreach($fact as $key=>$value){
    $timeline[$value['idtimeline']]['facts'][] = $value;
  }
  $range = $dateStop-$dateStart;
  $days = $range/86400;
  $dates = [];
  for($i=0;$i<=$days-1;$i++){
    $date = date("d.m.Y",$dateStart+($i*86400));
    $dates[$date] = range(0,23);
  }
  return ['timeline'=>$timeline,'dates'=>$dates];
},'PUBLIC'];
?>
