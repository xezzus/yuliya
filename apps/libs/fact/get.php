<?php
return function($dateStart,$dateStop){
  $db = $this->db->pg();
  $sql = "select idtimeline,dateleft,dateright,role.name as role from fact,role where dateleft >= :dateleft and dateright <= :dateright and plan.idrole = role.id";
  $sql = $db->prepare($sql);
  $sql->execute([':dateleft'=>$dateStart,':dateright'=>$dateStop]);
  $res = $sql->fetchAll();
  if($res === false) return false;
  else return $res;
}
?>
