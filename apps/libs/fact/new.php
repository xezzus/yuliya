<?php
return [function($idtimeline,$dateleft,$dateright,$idrole){
  $db = $this->db->pg();
  $sql = "insert into fact (idtimeline,dateleft,dateright,idrole) values (:idtimeline,:dateleft,:dateright,:idrole);";
  $sql = $db->prepare($sql);
  $sql->execute([':idtimeline'=>$idtimeline,':dateleft'=>$dateleft,':dateright'=>$dateright,':idrole'=>$idrole]);
  $err = $sql->errorInfo();
  if($err[0] == '00000') return true;
  else return false;
},'PRIVATE'];
?>
