<?php
return [function($employee,$role,$restaurant,$dateleft,$dateright){
  $idEmployee = $this->employee->getIdByName($employee);
  if($idEmployee === false){
    $idEmployee = $this->employee->new($employee);
    if($idEmployee === false) return 'ERROR';
  }
  $idRestaurant = $this->restaurant->getIdByName($restaurant);
  if($idRestaurant === false){
    $idRestaurant = $this->restaurant->new($restaurant);
    if($idRestaurant === false) return 'ERROR';
  }
  $idRole = $this->role->getIdByName($role);
  if($idRole === false){
    $idRole = $this->role->new($role);
    if($idRole === false) return 'ERROR';
  }
  $idtimeline = $this->timeline->getIdByName($idEmployee,$idRestaurant,$idRole);
  if($idtimeline === false){
    $idtimeline = $this->timeline->new($idEmployee,$idRestaurant,$idRole);
    if($idtimeline === false) return 'ERROR';
  }
  $res = $this->fact->new($idtimeline,$dateleft,$dateright,$idRole);
  if($res === false) return 'ERROR';
  else return true;
},'PUBLIC'];
?>
