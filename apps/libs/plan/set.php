<?php
return [function($employee,$role,$restaurant,$dateleft,$dateright){
  $idEmployee = $this->employee->getIdByName($employee);
  if($idEmployee === false){
    $idEmployee = $this->employee->new($employee);
    if($idEmployee === false) return 'ERROR 1';
  }
  $idRestaurant = $this->restaurant->getIdByName($restaurant);
  if($idRestaurant === false){
    $idRestaurant = $this->restaurant->new($restaurant);
    if($idRestaurant === false) return 'ERROR 2';
  }
  $idRole = $this->role->getIdByName($role);
  if($idRole === false){
    $idRole = $this->role->new($role);
    if($idRole === false) return 'ERROR 3';
  }
  $idtimeline = $this->timeline->getIdByName($idEmployee,$idRestaurant,$idRole);
  if($idtimeline === false){
    $idtimeline = $this->timeline->new($idEmployee,$idRestaurant,$idRole);
    if($idtimeline === false) return 'ERROR 4';
  }
  $res = $this->plan->new($idtimeline,$dateleft,$dateright,$idRole);
  if($res === false) return 'ERROR 5';
  else return true;
},'PUBLIC'];
?>
