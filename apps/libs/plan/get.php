<?php
return function($dateStart,$dateStop){
  $db = $this->db->pg();
  $sql = "select idtimeline,dateleft,dateright,role.name as role from plan,role where dateleft >= :dateleft and dateright <= :dateright and plan.idrole = role.id";
  $sql = $db->prepare($sql);
  $sql->execute([':dateleft'=>$dateStart,':dateright'=>$dateStop]);
  $res = $sql->fetchAll();
  foreach($res as $key=>$value){
    $res[$key]['size'] = $value['dateright']-$value['dateleft'];
    $res[$key]['hours'] = $res[$key]['size']/1200;
  }
  if($res === false) return false;
  else { return $res; }
}
?>
