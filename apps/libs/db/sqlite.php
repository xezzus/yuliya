<?php
return [function(){
  $file = __DIR__.'/db.sqlite';
  $db = new PDO('sqlite:'.$file);
  $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
  return $db;
},'SINGLE']
?>
