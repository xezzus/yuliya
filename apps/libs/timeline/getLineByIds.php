<?php
return function($ids){
  if(empty($ids)) return [];
  $db = $this->db->pg();
  $sql = "select timeline.id as idtimeline,employee.name as employee,restaurant.name as restaurant from timeline,employee,restaurant where timeline.id in ('".implode("','",$ids)."') and timeline.idemployee = employee.id and timeline.idrestaurant = restaurant.id limit ".count($ids);
  $res = $db->query($sql)->fetchAll();
  if($res === false) return false;
  else {
    $res = array_column($res,null,'idtimeline');
    foreach($res as $key=>$value){
      $res[$key]['plans'] = [];
      $res[$key]['facts'] = [];
    }
    return $res;
  }
}
?>
