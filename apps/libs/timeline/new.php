<?php
return [function($idEmployee,$idRestaurant){
  $db = $this->db->pg();
  $sql = "insert into timeline (idEmployee,idRestaurant) values (:idEmployee,:idRestaurant);";
  $sql = $db->prepare($sql);
  $sql->execute([':idEmployee'=>$idEmployee,':idRestaurant'=>$idRestaurant]);
  $id = $this->timeline->getIdByName($idEmployee,$idRestaurant);
  if($id === false) return false;
  else return $id;
},'PRIVATE'];
?>
