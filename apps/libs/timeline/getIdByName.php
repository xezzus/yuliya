<?php
return [function($idEmployee,$idRestaurant){
  $db = $this->db->pg();
  $sql = "select id from timeline where idEmployee = :idEmployee and idRestaurant = :idRestaurant limit 1";
  $sql = $db->prepare($sql);
  $sql->execute([':idEmployee'=>$idEmployee,':idRestaurant'=>$idRestaurant]);
  $res = $sql->fetch();
  if($res === false) return false;
  else return $res['id'];
},'PRIVATE'];
?>
