<?php
return [function($name){
  $db = $this->db->pg();
  $sql = "insert into employee (name) values (:name);";
  $sql = $db->prepare($sql);
  $sql->execute([':name'=>$name]);
  $id = $this->employee->getIdByName($name);
  if($id === false) return false;
  else return $id;
},'PRIVATE'];
?>
