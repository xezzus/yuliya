<?php
return [function($name){
  $db = $this->db->pg();
  $sql = "select id from role where name = :name limit 1";
  $sql = $db->prepare($sql);
  $sql->execute([':name'=>$name]);
  $res = $sql->fetch();
  if($res === false) return false;
  else return $res['id'];
},'PRIVATE'];
?>
