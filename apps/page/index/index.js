$(function () {

  $("[name=date]").daterangepicker({
      "dateLimit": {
          "days": 1
      },
      "linkedCalendars": false,
      "autoUpdateInput": false,
      "opens": "center",
      "drops": "down"
  }, function(start, end, label) {
    $("[name=date]").val(start.format('YYYY-MM-DD')+' to '+end.format('YYYY-MM-DD')).attr('data-dateStart',start.format('YYYY-MM-DD')).attr('data-dateStop',end.format('YYYY-MM-DD'));
  }).attr('readonly',true);

  // GET DATE
  var getDateStart = function(){
    var d = $( "[name=date]" ).data('datestart');
    var d = new Date(d);
    return d.getTime()/1000;
  };

  var getDateStop = function(){
    var d = $( "[name=date]" ).data('datestop');
    var d = new Date(d);
    return d.getTime()/1000;
  };

  // GET TIMELINE
  setInterval(function(){
    var d = $( "[name=date]" ).val();
    var date = d.split(' to ');
    var dateStart = new Date(date[0]);
    var dateStart = dateStart.getTime()/1000;
    var dateStop = new Date(date[1]);
    var dateStop = (dateStop.getTime()/1000)+86400;
    apps.name('filter/get').data({dateStart:dateStart,dateStop:dateStop}).exec(function(e){
      $('.timeline .header').empty();
      $('.timeline .header').append('<div class="lineHeader"><div class="employee">Сотрудник</div><div class="restaurant">Ресторан</div></div>');
      $('.timeline .times').empty();
      $('.timeline .times').append('<div class="lineHeader"><div class="days"></div><div class="hours"></div></div>');
      for(i in e.timeline){
        var info = e.timeline[i];
        $('.timeline .header').append('<div class="line"><div class="employee">'+info.employee+'</div><div class="restaurant">'+info.restaurant+'</div></div>');
        $('.timeline .times').append('<div class="line"></div>');
      }
      for(i in e.dates){
        var day = e.dates[i];
        for(x in day){
          var h = day[x];
          $('.timeline .times .line').append('<div class="hour"></div>');
          $('.timeline .times .lineHeader .hours').append('<div class="hour">'+h+'</div>');
        }
        $('.timeline .times .lineHeader .days').append('<div class="day">'+i+'</div>');
      }
      console.log(e);
    });
  },6000);

});
